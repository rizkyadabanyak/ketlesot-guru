package com.tgsiak.rizky.ketlesot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DaftarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
    }

    public void code (View v){

        Intent intent = new Intent(this,KodeActivity.class);
        startActivity(intent);
        DaftarActivity.this.finish();

    }
}
