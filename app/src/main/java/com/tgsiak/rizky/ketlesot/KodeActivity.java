package com.tgsiak.rizky.ketlesot;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class KodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kode);
        TextView logofont = findViewById(R.id.font1);
        Typeface custom_fonts = Typeface.createFromAsset(getAssets(), "fonts/ArgonPERSONAL-Regular.otf");
        logofont.setTypeface(custom_fonts);
    }

    public void back (View view){
        Intent intent = new Intent(this,DaftarActivity.class);
        startActivity(intent);
        KodeActivity.this.finish();

    }
    public void finish (View view){
        Intent intent = new Intent(this,RoomActivity.class);
        startActivity(intent);
        KodeActivity.this.finish();

    }
}
